<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('referencia',50)->unique();
            $table->unsignedInteger('id_repartidor');
            $table->unsignedInteger('id_cliente');
            $table->unsignedInteger('id_estado');
            $table->unsignedInteger('id_direccion');
            $table->timestamps();
        });

        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('id_direccion')->references('id')->on('direcciones')->onDelete('cascade');
        });

        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('id_repartidor')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('id_cliente')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('id_estado')->references('id')->on('estados')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
