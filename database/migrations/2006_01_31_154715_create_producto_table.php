<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->unsignedInteger('categoria');
            $table->string('descripcion_1',500);
            $table->string('descripcion_2',1000);
            $table->string('precio',30);
            $table->string('image',1000);
            $table->unsignedInteger('proveedor');
            $table->integer('stock');
            $table->timestamps();
        });

        Schema::table('productos', function (Blueprint $table) {
            $table->foreign('proveedor')->references('id')->on('proveedores')->onDelete('cascade');
        });

        Schema::table('productos', function (Blueprint $table) {
            $table->foreign('categoria')->references('id')->on('categorias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
