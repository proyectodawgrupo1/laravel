<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array(1,2,3);
        $faker = \Faker\Factory::create();
        foreach ($a as $key => $value) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => now(),
                'password' => "$2y$10$5rIJ2CujSFehOWQTfjmAteqfpd3s62CmDOzmi2L98ArCA.QTQ0KPy",  //12345678
                'remember_token' => Str::random(10),
                'id_rol' => $value
            ]);
        }
    }
}
