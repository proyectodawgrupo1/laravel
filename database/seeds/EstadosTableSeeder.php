<?php

use App\Estado;
use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array("Preparación","Preparado","En ruta","Entregado","No entregado","Cancelado");
        foreach ($a as $key => $value) {
            Estado::create([
                'name' => $value 
            ]);
        }
    }
}
