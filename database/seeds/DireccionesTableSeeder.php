<?php

use App\Direccion;
use Illuminate\Database\Seeder;

class DireccionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array("Proveedor de Castalla","Proveedor de IBI","Proveedor de Alcoy");
        foreach ($a as $key => $value) {
            Direccion::create([
                'id_user' => $key+1,
                'name' => "Nombre de prueba nº".$key,
                'direccion_1' => "Avenida nº 1".$key,
                'direccion_2' => "Calle nº 1".$key,
                'ciudad' => "Alicante",
                'provincia' => "Alicante",
                'pais' => "España",
                'telefono' => "63708899".$key
            ]);
        }
    }
}
