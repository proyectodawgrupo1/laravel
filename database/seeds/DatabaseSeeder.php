<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriasTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(EstadosTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(DireccionesTableSeeder::class);
        $this->call(PedidosTableSeeder::class);
        $this->call(ProductosTableSeeder::class);
        $this->call(LinPedsTableSeeder::class);
    }
}
