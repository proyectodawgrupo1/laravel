<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array("Administrador","Repartidor","Cliente");
        foreach ($a as $key => $value) {
            Role::create([
                'name' => $value 
            ]);
        }
    }
}
