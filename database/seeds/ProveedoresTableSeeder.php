<?php

use App\Proveedor;
use Illuminate\Database\Seeder;

class ProveedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array("Proveedor de Castalla","Proveedor de IBI","Proveedor de Alcoy");
        foreach ($a as $key => $value) {
            Proveedor::create([
                'name' => $value,
                'descripcion' => "Descripcion de prueba nº 1".$key,
                'telefono' => "64987634".$key,
                'direccion' => "Avenida nº 1".$key
            ]);
        }
    }
}
