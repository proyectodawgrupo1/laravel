<?php

use App\Pedido;
use Illuminate\Database\Seeder;

class PedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array(1,2,3);
        foreach ($a as $key => $value) {
            Pedido::create([
                'referencia' => $this->generateRandomString(25),
                'id_repartidor' => 2,
                'id_cliente' => $key+1,
                'id_estado' => $key+1,
                'id_direccion' => $key+1
            ]);
        }
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
