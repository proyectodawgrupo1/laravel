<?php

use App\LinPed;
use Illuminate\Database\Seeder;

class LinPedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array(1,2,3);
        foreach ($a as $value) {
            LinPed::create([
                'id_pedido' => $value,
                'id_producto' => $value,
                'cantidad' => "1".$value
            ]);
        }
    }
}
