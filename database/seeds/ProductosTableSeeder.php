<?php

use App\Producto;
use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array("Toalla","Neumaticos","Recetas de comida","Sofa","Cuadro Vintage");
        foreach ($a as $key => $value) {
            Producto::create([
                'name' => $value,
                'categoria' => $key+1,
                'descripcion_1' => "Primera descripcion - corta nº 1".$key,
                'descripcion_2' => "Segunda descripcion - larga nº 1".$key,
                'precio' => "1".$key,
                'image' => "https://dam.ngenespanol.com/wp-content/uploads/2019/03/luna-colores-nuevo.png",
                'proveedor' => 2,
                'stock' => "1".$key
            ]);
        }
    }
}
