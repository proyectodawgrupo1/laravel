<?php

use App\Categoria;
use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = array("Hogar","Coches","Cocina","Muebles","Decoración");
        foreach ($a as $key => $value) {
            Categoria::create([
                'name' => $value 
            ]);
        }
    }
}
