# Backoffice + Api

## Descripciòn

Aplicacion de acceso a la base de datos a traves de API, control de las tablas maestras. 

## Dependencias

* [Laravel 6](https://laravel.com/docs/6.x) 
* [Passport](https://laravel.com/docs/6.x/passport)

## WIKI

[enlace](https://bitbucket.org/proyectodawgrupo1/laravel/wiki/Home)

## Integrantes

* David González Argueta
* Carlos Robles Maranchón
* Adrián Meneses Ruíz
* Zaira Bravo Sánchez
