@include('head.head')
@include('header.header')
<section class="container-fluid p-5">
    <h1>Cuenta</h1>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">{{Auth::user()->name}}</h5>
            <p class="card-text">Información sobre el usuario</p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">{{Auth::user()->email}}</li>
            <li class="list-group-item">Rol Administrador</li>
            <li class="list-group-item">ID de usuario: {{Auth::user()->id}}</li>
        </ul>
    </div>
</section>
@include('links.links')