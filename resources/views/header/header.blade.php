<nav class="header navbar navbar-expand-lg navbar-light bg-light">
  <img class="logo" src="https://i.imgur.com/5sc8HU1.png" alt="Fondo">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="home">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="pedidos">Pedidos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="productos">Productos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="usuarios">Usuarios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="proveedores">Proveedores</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categorias">Categorias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="estados">Estados</a>
      </li>
        @auth
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{Auth::user()->name}}
                </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="cuenta">Perfil</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        @endauth
    </ul>
  </div>
</nav>