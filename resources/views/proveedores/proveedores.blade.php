@include('head.head')
@include('header.header')
<section class="container-fluid p-5">
    <h1>Proveedores<a class="btn btn-outline-success ml-3 mb-2" href="/add-prov">Añadir proveedor</a></h1>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Descripcion</th>
            <th scope="col">Telefono</th>
            <th scope="col">Direccion</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($proveedores as $proveedor)
            <tr>
                <th scope="row">{{$proveedor->id}}</th>
                <td>{{$proveedor->name}}</td>
                <td>{{$proveedor->descripcion}}</td>
                <td>{{$proveedor->telefono}}</td>
                <td>{{$proveedor->direccion}}</td>
                <td>
                    <a class="btn btn-outline-primary mr-1" style="float:left" href="/mod-prov/{{$proveedor->id}}">Editar</a>
                    <form method="POST" action="/del-prov/{{$proveedor->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-danger" value="Borrar">
                        </div>
                    </form> 
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</section>
@include('links.links')