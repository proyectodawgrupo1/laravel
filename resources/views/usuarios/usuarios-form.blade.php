@include('head.head')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if(!isset($id))
                <div class="card-header">Agregar Usuario</div>
            @else
                <div class="card-header">Editar Usuario</div>
            @endif

                <div class="card-body">
                @if(!isset($id))
                    <form method="POST" action="{{ route('add-user') }}">
                        @csrf
                @else
                    <form method="POST" action="{{ route('mod-user') }}">
                        @csrf
                        <div class="form-group row">
                            <label class="form-control col-md-3 ml-4">ID</label>
                            <input name="id" class="form-control col-md-3" type="text" value="{{$id}}" readonly>
                        </div>
                @endif

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Nombre</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-3 mt-2" name="name" required>
                            @else
                                <input type="text" class="form-control col-md-3 mt-2" name="name" value="{{$usuario->name}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Email</label>
                            @if(!isset($id))
                                <input type="email" class="form-control col-md-3 mt-2" name="email" required>
                            @else
                                <input type="email" class="form-control col-md-3 mt-2" name="email" value="{{$usuario->email}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Password</label>
                            <input type="password" class="form-control col-md-3 mt-2" name="password" value="12345678" required>
                        </div>

                        <div class="form-group row">
                            <label for="id_rol" class="col-md-5 col-form-label text-md-right">Rol</label>
                            <select name="id_rol">
                                @foreach($roles as $rol)
                                    <option value="{{$rol->id}}">{{$rol->name}}</option>
                                @endforeach 
                            </select>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                 <button type="submit" class="btn btn-primary btn-block">
                                  @if(!isset($id))
                                     Agregar
                                  @else
                                     Editar
                                  @endif
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('links.links')
