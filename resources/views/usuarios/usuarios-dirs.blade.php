@include('head.head')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if(!isset($id))
                <div class="card-header">Agregar dirección a usuario</div>
            @else
                <div class="card-header">Editar dirección a usuario</div>
            @endif

                <div class="card-body">
                @if(!isset($id))
                    <form method="POST" action="{{ route('add-dir-user') }}">
                        @csrf
                @else
                    <form method="POST" action="{{ route('mod-dir-user') }}">
                        @csrf
                @endif
                        <div class="form-group row">
                            @if(!isset($id))
                            <label class="col-md-3 m-3">ID de usuario:</label>
                                <input name="id_user" class="form-control col-md-3 mt-2" type="text" placeholder="{{$user->id}}" value="{{$user->id}}" readonly>
                            @else
                            <label class="col-md-3 m-3">ID de usuario y de dirección:</label>
                                <input name="id_user" class="form-control col-md-3 mt-2" type="text" placeholder="{{$direccion->id_user}}" value="{{$direccion->id_user}}" readonly>
                                <input name="id" class="form-control col-md-3 mt-2" type="text" placeholder="{{$id}}" value="{{$id}}" readonly>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Nombre</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-6 mt-2" name="name" required>
                            @else
                                <input type="text" class="form-control col-md-6 mt-2" name="name" value="{{$direccion->name}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Primera dirección</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-6 mt-2" name="direccion_1" required>
                            @else
                                <input type="text" class="form-control col-md-6 mt-2" name="direccion_1" value="{{$direccion->direccion_1}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Segunda dirección</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-6 mt-2" name="direccion_2" required>
                            @else
                                <input type="text" class="form-control col-md-6 mt-2" name="direccion_2" value="{{$direccion->direccion_2}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Ciudad</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-6 mt-2" name="ciudad" required>
                            @else
                                <input type="text" class="form-control col-md-6 mt-2" name="ciudad" value="{{$direccion->ciudad}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Provincia</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-6 mt-2" name="provincia" required>
                            @else
                                <input type="text" class="form-control col-md-6 mt-2" name="provincia" value="{{$direccion->provincia}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Pais</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-6 mt-2" name="pais" required>
                            @else
                                <input type="text" class="form-control col-md-6 mt-2" name="pais" value="{{$direccion->pais}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Telefono</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-6 mt-2" name="telefono" required>
                            @else
                                <input type="text" class="form-control col-md-6 mt-2" name="telefono" value="{{$direccion->telefono}}" required>
                            @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                 <button type="submit" class="btn btn-primary btn-block">
                                  @if(!isset($id))
                                     Agregar dirección
                                  @else
                                     Editar dirección
                                  @endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('links.links')
