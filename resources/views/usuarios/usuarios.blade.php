@include('head.head')
@include('header.header')
<section class="container-fluid p-5">
    <h1>Usuarios<a class="btn btn-outline-success ml-3 mb-2" href="/add-usuario">Añadir usuario</a></h1>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Rol</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)
            <tr>
                <th scope="row">{{$usuario->id}}</th>
                <td>{{$usuario->name}}</td>
                <td>{{$usuario->email}}</td>
                <td>{{$usuario->rol->name}}</td>
                <td>
                    <a class="btn btn-outline-success" href="/add-dir-user/{{$usuario->id}}">Nueva direccion</a>
                    <button class="btn btn-outline-info" data-toggle="collapse" data-target="#demo1-{{$usuario->id}}" class="accordion-toggle">Direcciones</button>
                    <a class="btn btn-outline-primary m-1" href="/mod-usuario/{{$usuario->id}}">Editar</a>
                    <form method="POST" action="/del-usuario/{{$usuario->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-danger mt-2" value="Borrar">
                        </div>
                    </form>
                </td>
            </tr>
            @foreach($usuario->direcciones as $direccion)
                <tr>
                    <td colspan="7">Dirección: {{$direccion->name}} | ID: {{$direccion->id}}
                        <div class="collapse" id="demo1-{{$usuario->id}}">
                            <hr>
                                <p>Dirección 1 : <span>{{$direccion->direccion_1}}</span> </p>
                                <p>Dirección 2 : <span>{{$direccion->direccion_2}}</span> </p>
                                <p>Ciudad : <span>{{$direccion->ciudad}}</span> </p>
                                <p>Provincia : <span>{{$direccion->provincia}}</span> </p>
                                <p>Pais : <span>{{$direccion->pais}}</span> </p>
                                <p>Telefono : <span>{{$direccion->telefono}}</span> </p>
                                <a class="btn btn-outline-primary mr-1" style="float:left" href="/mod-dir-user/{{$direccion->id}}">Editar</a>
                                <form method="POST" action="/del-dir-user/{{$direccion->id}}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-outline-danger" value="Borrar">
                                    </div>
                                </form>
                            <hr>
                        </div> 
                    </td> 
                </tr>
            @endforeach
        @endforeach
    </tbody>
    </table>
</section>
@include('links.links')