@include('head.head')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if(!isset($id))
                <div class="card-header">Agregar Producto</div>
            @else
                <div class="card-header">Editar Producto</div>
            @endif

                <div class="card-body">
                @if(!isset($id))
                    <form method="POST" action="{{ route('add-producto') }}">
                        @csrf
                @else
                    <form method="POST" action="{{ route('mod-producto') }}">
                        @csrf
                        <div class="form-group row">
                            <label class="form-control col-md-3 ml-4">ID</label>
                            <input name="id" class="form-control col-md-3" type="text" value="{{$id}}" readonly>
                        </div>
                @endif
                        <div class="form-group row">
                            <label class="col-md-3 m-3">Nombre</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-3 mt-2" name="name" required>
                            @else
                                <input type="text" class="form-control col-md-3 mt-2" name="name" value="{{$producto->name}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Descripcion corta</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-3 mt-2" name="descripcion_1" required>
                            @else
                                <input type="text" class="form-control col-md-3 mt-2" name="descripcion_1" value="{{$producto->descripcion_1}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Descripcion larga</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-3 mt-2" name="descripcion_2" required>
                            @else
                                <input type="text" class="form-control col-md-3 mt-2" name="descripcion_2" value="{{$producto->descripcion_2}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Precio</label>
                            @if(!isset($id))
                                <input type="number" class="form-control col-md-3 mt-2" name="precio" min="1" step="0.01" required>
                            @else
                                <input type="number" class="form-control col-md-3 mt-2" name="precio" min="1" step="0.01" value="{{$producto->precio}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Imágen (URL)</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-3 mt-2" name="image" required>
                            @else
                                <input type="text" class="form-control col-md-3 mt-2" name="image" value="{{$producto->image}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Stock</label>
                            @if(!isset($id))
                                <input type="number" class="form-control col-md-3 mt-2" name="stock" min="0" required>
                            @else
                                <input type="number" class="form-control col-md-3 mt-2" name="stock" min="0" value="{{$producto->stock}}" required>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="categoria" class="col-md-5 col-form-label text-md-right">Categoria</label>
                            <select name="categoria">
                                @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->name}}</option>
                                @endforeach 
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="proveedor" class="col-md-5 col-form-label text-md-right">Proveedores</label>
                            <select name="proveedor">
                                @foreach($proveedores as $proveedor)
                                    <option value="{{$proveedor->id}}">{{$proveedor->name}}</option>
                                @endforeach 
                            </select>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                 <button type="submit" class="btn btn-primary btn-block">
                                  @if(!isset($id))
                                     Agregar
                                  @else
                                     Editar
                                  @endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('links.links')
