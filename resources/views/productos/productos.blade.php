@include('head.head')
@include('header.header')
<section class="container-fluid p-5">

    <h1>Productos 
        <a class="btn btn-outline-success ml-3 mb-3" href="/add-producto">Añadir producto</a>
        <a class="btn btn-outline-warning ml-1 mb-3" href="/export-excel">Exportar productos</a>
        <form action="{{ route('import') }}" method="post" enctype="multipart/form-data" class="btn">
            @csrf
            <input type="file" name="file" class="btn btn-outline-primary" required>
            <button class="btn btn-outline-primary ml-3">Importar productos</button>
        </form>

    </h1>

    <table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Categoria</th>
            <th scope="col">Descripción</th>
            <th scope="col">Precio</th>
            <th scope="col">Imagen</th>
            <th scope="col">Proveedor</th>
            <th scope="col">Stock</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($productos as $producto)
            <tr>
                <th scope="row">{{$producto->id}}</th>
                <td>{{$producto->name}}</td>
                <td>{{$producto->cat->name}}</td>
                <td>{{$producto->descripcion_2}}</td>
                <td>{{$producto->precio}}€</td>
                <td>
                    <img src="{{$producto->image}}" alt="Smiley face" height="42" width="62">
                </td>
                <td>{{$producto->prov->name}}</td>
                <td>{{$producto->stock}}</td>
                <td>
                    <a class="btn btn-outline-primary m-1" href="/mod-producto/{{$producto->id}}">Editar</a>
                    <form method="POST" action="/del-producto/{{$producto->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-danger m-1" value="Borrar">
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</section>
@include('links.links')