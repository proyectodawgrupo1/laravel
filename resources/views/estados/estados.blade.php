@include('head.head')
@include('header.header')
<section class="container-fluid p-5">
    <h1>Estados de pedidos<a class="btn btn-outline-success ml-3 mb-2" href="/add-estado">Añadir estado</a></h1>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($estados as $estado)
            <tr>
                <th scope="row">{{$estado->id}}</th>
                <td>{{$estado->name}}</td>
                <td>
                    <a class="btn btn-outline-primary mr-2" style="float:left" href="/mod-estado/{{$estado->id}}">Editar</a>
                    <form method="POST" action="/del-estado/{{$estado->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-danger" value="Borrar">
                        </div>
                    </form> 
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</section>
@include('links.links')