@include('head.head')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if(!isset($id))
                <div class="card-header">Agregar Pedido</div>
            @else
                <div class="card-header">Editar Pedido</div>
            @endif

                <div class="card-body">
                @if(!isset($id))
                    <form method="POST" action="{{ route('add-pedido') }}">
                        @csrf
                @else
                    <form method="POST" action="{{ route('mod-pedido') }}">
                        @csrf

                        <div class="form-group row">
                            <input name="id" class="form-control col-md-3 m-3" type="text" placeholder="{{$pedido->id}}" value="{{$pedido->id}}" readonly>
                            <input name="referencia" class="form-control col-md-6 m-3" type="text" placeholder="{{$pedido->referencia}}" value="{{$pedido->referencia}}" readonly>
                        </div>

                        <div class="form-group row">
                            <label for="id_estado" class="col-md-5 col-form-label text-md-right">Estado</label>
                            <select name="id_estado">
                                @foreach($estados as $estado)
                                    <option value="{{$estado->id}}">{{$estado->name}}</option>
                                @endforeach 
                            </select>
                        </div>
                @endif
                        <div class="form-group row">
                            <label for="id_repartidor" class="col-md-5 col-form-label text-md-right">Repartidor</label>
                            <select name="id_repartidor">
                                @foreach($repartidores as $repartidor)
                                    <option value="{{$repartidor->id}}">{{$repartidor->name}}</option>
                                @endforeach 
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="id_cliente" class="col-md-5 col-form-label text-md-right">Cliente</label>
                            <select name="id_cliente">
                                @foreach($clientes as $cliente)
                                    <option value="{{$cliente->id}}">{{$cliente->name}}</option>
                                @endforeach 
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="id_direccion" class="col-md-5 col-form-label text-md-right">Direccion</label>
                            <select name="id_direccion">
                                @foreach($direcciones as $direccion)
                                    <option value="{{$direccion->id}}">{{$direccion->name}}</option>
                                @endforeach 
                            </select>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                 <button type="submit" class="btn btn-primary btn-block">
                                  @if(!isset($id))
                                     Agregar
                                  @else
                                     Editar
                                  @endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('links.links')
