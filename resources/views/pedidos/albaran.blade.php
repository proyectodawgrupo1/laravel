<h1>Albarán de pedido</h1>
<?php $sum = 0; ?>
<table>
  <thead>
    <tr>
        <th>ID</th>
        <th>Referencia</th>
        <th>Repartidor</th>
        <th>Cliente</th>
        <th>Estado</th>
        <th>Direccion</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>{{ $pedido->id }}</td>
        <td>{{ $pedido->referencia }}</td>
        <td>{{ $pedido->repartidor->name }}</td>
        <td>{{ $pedido->cliente->name }}</td>
        <td>{{ $pedido->estado->name }}</td>
        <td>{{ $pedido->direccion->name }}</td>
    </tr>
  </tbody>
</table>
<table>
  <thead>
    <tr>
        <th>LÍnea</th>
        <th>Producto</th>
        <th>Cantidad</th>
        <th>Precio</th>
        <th>Importe</th>
    </tr>
  </thead>
  <tbody>
    @foreach($pedido->lineas as $linea)
        <tr>
            <td>{{ $linea->id }}</td>
            <td>{{ $linea->producto->name }}</td>
            <td>{{ $linea->cantidad }}</td>
            <td>{{ $linea->producto->precio }} €</td>
            <td>{{ $linea->producto->precio * $linea->cantidad }} €</td>
            <?php $sum += $linea->producto->precio * $linea->cantidad; ?>
        </tr>
    @endforeach
  </tbody>
</table>

<table>
  <thead>
    <tr>
        <th>Total a pagar: {{$sum}} €</th>
    </tr>
  </thead>
</table>

<p>
    <a href="/pedidos-pdf-descargar/{{ $pedido->id }}" style="margin:20px">
        Descargar albarán en PDF
    </a>
</p>

<style>
body {
  font-family: "Tahoma", "Geneva", "sans-serif";
}
table {
  width:100%;
  margin-bottom: 20px;
  background-color: #ccffff;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 15px;
  text-align: left;
}
</style>
