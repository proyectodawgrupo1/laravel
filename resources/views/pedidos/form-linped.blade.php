@include('head.head')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if(!isset($id))
                <div class="card-header">Agregar Linea de Pedido</div>
            @else
                <div class="card-header">Editar Linea de Pedido</div>
            @endif

                <div class="card-body">
                @if(!isset($id))
                    <form method="POST" action="{{ route('add-lin-pedido') }}">
                        @csrf
                @else
                    <form method="POST" action="{{ route('mod-lin-pedido') }}">
                        @csrf
                @endif
                        <div class="form-group row">
                            @if(!isset($id))
                            <label class="col-md-3 m-3">ID de pedido:</label>
                                <input name="id_pedido" class="form-control col-md-3 mt-2" type="text" placeholder="{{$pedido->id}}" value="{{$pedido->id}}" readonly>
                            @else
                            <label class="col-md-3 m-3">ID de pedido y de linea:</label>
                                <input name="id_pedido" class="form-control col-md-3 mt-2" type="text" placeholder="{{$linPed->id_pedido}}" value="{{$linPed->id_pedido}}" readonly>
                                <input name="id" class="form-control col-md-3 mt-2" type="text" placeholder="{{$id}}" value="{{$id}}" readonly>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="id_producto" class="col-form-label m-2">Producto</label>
                            <select name="id_producto">
                                @foreach($productos as $producto)
                                    <option value="{{$producto->id}}">{{$producto->name}}</option>
                                @endforeach 
                            </select>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Cantidad</label>
                            @if(!isset($id))
                                <input type="number" class="form-control col-md-3 mt-2" name="cantidad" min="1" required>
                            @else
                                <input type="number" class="form-control col-md-3 mt-2" name="cantidad" min="1" value="{{$linPed->cantidad}}" required>
                            @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                 <button type="submit" class="btn btn-primary btn-block">
                                  @if(!isset($id))
                                     Agregar
                                  @else
                                     Editar
                                  @endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('links.links')
