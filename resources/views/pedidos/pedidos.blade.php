@include('head.head')
@include('header.header')
<section class="container-fluid p-5">
    <h1>Pedidos<a class="btn btn-outline-success ml-3 mb-2" href="add-pedido">Añadir pedido</a></h1>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Referencia</th>
            <th scope="col">Repartidor</th>
            <th scope="col">Cliente</th>
            <th scope="col">Estado</th>
            <th scope="col">Direccion</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pedidos as $pedido)
            <tr>
                <th scope="row">{{$pedido->id}}</th>
                <td>{{$pedido->referencia}}</td>
                <td>{{$pedido->repartidor->name}}</td>
                <td>{{$pedido->cliente->name}}</td>
                <td>{{$pedido->estado->name}}</td>
                <td>{{$pedido->direccion->name}}</td>
                <td>
                    <a class="btn btn-outline-success" href="/add-lin-pedido/{{$pedido->id}}">Nueva linea</a>
                    <button class="btn btn-outline-info" data-toggle="collapse" data-target="#demo1-{{$pedido->id}}" class="accordion-toggle">Lineas de Pedido</button>
                    <a class="btn btn-outline-primary" href="/mod-pedido/{{$pedido->id}}">Editar</a>
                    <a class="btn btn-outline-warning" href="/pedidos-pdf-show/{{$pedido->id}}">Albarán</a>
                    <form method="POST" action="/del-pedido/{{$pedido->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-danger" style="float:left" value="Eliminar pedido">
                        </div>
                    </form>
                </td>
            </tr>
            @foreach($pedido->lineas as $linea)
                <tr>
                    <td colspan="7">Lineas de pedido
                        <div class="collapse" id="demo1-{{$pedido->id}}">
                            <hr>
                                <p>Producto : <span>{{$linea->producto->name}} | ID : {{$linea->producto->id}}</span> </p>
                                <p>Precio : <span>{{$linea->producto->precio}}€</span> </p>
                                <p>Cantidad : <span>{{$linea->cantidad}}</span> </p>
                                <a class="btn btn-outline-primary mr-1" style="float:left" href="/mod-lin-pedido/{{$linea->id}}">Editar</a>
                                <form method="POST" action="/del-lin-pedido/{{$linea->id}}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-outline-danger" value="Borrar">
                                    </div>
                                </form>
                            <hr>
                        </div> 
                    </td> 
                </tr>
            @endforeach
        @endforeach
    </tbody>
    </table>
</section>
@include('links.links')