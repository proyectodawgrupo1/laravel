@include('head.head')
@include('header.header')
<section class="container-fluid p-5">
    <h1>Categorias<a class="btn btn-outline-success ml-3 mb-2" href="/add-cat">Añadir categoría</a></h1>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categorias as $categoria)
            <tr>
                <th scope="row">{{$categoria->id}}</th>
                <td>{{$categoria->name}}</td>
                <td>
                    <a class="btn btn-outline-primary mr-2" style="float:left" href="/mod-cat/{{$categoria->id}}">Editar</a>
                    <form method="POST" action="/del-cat/{{$categoria->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-danger" value="Borrar">
                        </div>
                    </form> 
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</section>
@include('links.links')