@include('head.head')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if(!isset($id))
                <div class="card-header">Agregar Categoría</div>
            @else
                <div class="card-header">Editar Categoría</div>
            @endif

                <div class="card-body">
                @if(!isset($id))
                    <form method="POST" action="{{ route('add-cat') }}">
                        @csrf
                @else
                    <form method="POST" action="{{ route('mod-cat') }}">
                        @csrf

                        <div class="form-group row">
                            <input class="form-control col-md-3 m-3" type="text" placeholder="ID" value="ID" readonly>
                            <input name="id" class="form-control col-md-3 m-3" type="text" placeholder="{{$cat->id}}" value="{{$cat->id}}" readonly>
                        </div>
                @endif

                        <div class="form-group row">
                            <label class="col-md-3 m-3">Nombre</label>
                            @if(!isset($id))
                                <input type="text" class="form-control col-md-3 mt-2" name="name" required>
                            @else
                                <input type="text" class="form-control col-md-3 mt-2" name="name" value="{{$cat->name}}" required>
                            @endif
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                 <button type="submit" class="btn btn-primary btn-block">
                                  @if(!isset($id))
                                     Agregar
                                  @else
                                     Editar
                                  @endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('links.links')
