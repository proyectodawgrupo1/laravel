<?php

namespace Tests\Feature;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EstadosTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testEstados()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create([
            'email' => $faker->unique()->safeEmail,
            'id_rol' => 1
        ]);

        $estado = [
            'name' => 'estado_x'
        ];

        $modEstado = [
            'name' => 'estado_x_modificacion'
        ];

        $token = $user->createToken('id')->accessToken;
        $headers = ['Authorization' => "Bearer ".$token];

        $this->json('get', 'api/estados', [], $headers)->assertStatus(200);
        $this->json('get', 'api/estados/1', [], $headers)->assertStatus(200);
        $this->json('post', 'api/estados', $estado, $headers)->assertStatus(201);
        $this->json('put', 'api/estados/7', $modEstado, $headers)->assertStatus(200);
        $this->json('delete', 'api/estados/7', [], $headers)->assertStatus(200);
        $this->json('get', 'api/logout', [], $headers)->assertStatus(200);
    }
}
