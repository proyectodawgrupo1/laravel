<?php

namespace Tests\Feature;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PedidosTest extends TestCase
{

    public function testPedidos()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create([
            'email' => $faker->unique()->safeEmail,
            'id_rol' => 1
        ]);

        $pedido = [
            'id_repartidor' => 2,
            'id_direccion' => 3,
            'id_cliente' => 3
        ];

        $modPedido = [
            'id_repartidor' => 2,
            'id_direccion' => 3,
            'id_cliente' => 3,
            'id_estado' => 2
        ];

        $token = $user->createToken('id')->accessToken;
        $headers = ['Authorization' => "Bearer ".$token];

        $this->json('get', 'api/user', [], $headers)->assertStatus(200);
        $this->json('get', 'api/pedidos', [], $headers)->assertStatus(200);
        $this->json('get', 'api/pedidos/1', [], $headers)->assertStatus(200);
        $this->json('post', 'api/pedidos', $pedido, $headers)->assertStatus(201);
        $this->json('put', 'api/pedidos/4', $modPedido, $headers)->assertStatus(200);
        $this->json('delete', 'api/pedidos/4', [], $headers)->assertStatus(200);
        $this->json('get', 'api/logout', [], $headers)->assertStatus(200);

    }

}
