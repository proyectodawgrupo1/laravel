<?php

namespace Tests\Feature;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductosTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testProductos()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create([
            'email' => $faker->unique()->safeEmail,
            'id_rol' => 1
        ]);

        $producto = [
            'name' => 'ejemplo',
            'categoria' => 1,
            'descripcion_1' => 'descripcion_1',
            'descripcion_2' => 'descripcion_1',
            'precio' => '10',
            'image' => 'image',
            'proveedor' => 1,
            'stock' => 1
        ];

        $modProducto = [
            'name' => 'modificacion',
            'categoria' => 1,
            'descripcion_1' => 'modificacion_descripcion_1',
            'descripcion_2' => 'modificacion_descripcion_1',
            'precio' => '10',
            'image' => 'image',
            'proveedor' => 1,
            'stock' => 1
        ];

        $token = $user->createToken('id')->accessToken;
        $headers = ['Authorization' => "Bearer ".$token];

        $this->json('get', 'api/productos', [], $headers)->assertStatus(200);
        $this->json('get', 'api/productos/1', [], $headers)->assertStatus(200);
        $this->json('post', 'api/productos', $producto, $headers)->assertStatus(201);
        $this->json('put', 'api/productos/6', $modProducto, $headers)->assertStatus(200);
        $this->json('delete', 'api/productos/6', [], $headers)->assertStatus(200);
        $this->json('get', 'api/logout', [], $headers)->assertStatus(200);
    }
}
