<?php

namespace Tests\Feature;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UsersTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUsuarios()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create([
            'email' => $faker->unique()->safeEmail,
            'id_rol' => 1
        ]);

        $usuario = [
            'name' => 'ejemplo',
            'email' => $faker->unique()->safeEmail,
            'password' => '1234',
            'id_rol' => 3
        ];

        $modUsuario = [
            'name' => 'modificacion',
            'email' => $faker->unique()->safeEmail,
            'password' => '1234',
            'id_rol' => 3
        ];

        $token = $user->createToken('id')->accessToken;
        $headers = ['Authorization' => "Bearer ".$token];

        $this->json('get', 'api/users', [], $headers)->assertStatus(200);
        $this->json('get', 'api/users/1', [], $headers)->assertStatus(200);
        $this->json('post', 'api/users', $usuario, $headers)->assertStatus(201);
        $this->json('put', 'api/users/4', $modUsuario, $headers)->assertStatus(200);
        $this->json('delete', 'api/users/4', [], $headers)->assertStatus(200);
        $this->json('get', 'api/logout', [], $headers)->assertStatus(200);
    }
}
