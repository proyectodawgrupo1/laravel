<?php

namespace Tests\Feature;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DireccionesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create([
            'email' => $faker->unique()->safeEmail,
            'id_rol' => 1
        ]);

        $direccion = [
            'id_user' => 3,
            'name' => 'direccion',
            'direccion_1' => 'direccion_1',
            'direccion_2' => 'direccion_2',
            'ciudad' => 'ciudad',
            'provincia' => 'provincia',
            'pais' => 'pais',
            'telefono' => '123456789'
        ];

        $modDireccion = [
            'id_user' => 3,
            'name' => 'direccionModificada',
            'direccion_1' => 'direccion_1',
            'direccion_2' => 'direccion_2',
            'ciudad' => 'ciudad',
            'provincia' => 'provincia',
            'pais' => 'pais',
            'telefono' => '123456789'
        ];

        $token = $user->createToken('id')->accessToken;
        $headers = ['Authorization' => "Bearer ".$token];

        $this->json('get', 'api/direcciones', [], $headers)->assertStatus(200);
        $this->json('get', 'api/direcciones/1', [], $headers)->assertStatus(200);
        $this->json('post', 'api/direcciones', $direccion, $headers)->assertStatus(201);
        $this->json('put', 'api/direcciones/4', $modDireccion, $headers)->assertStatus(200);
        $this->json('delete', 'api/direcciones/4', [], $headers)->assertStatus(200);
        $this->json('get', 'api/logout', [], $headers)->assertStatus(200);
    }
}
