<?php

namespace Tests\Feature;

use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoriasTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCategorias()
    {
        $faker = Factory::create();

        $user = factory(User::class)->create([
            'email' => $faker->unique()->safeEmail,
            'id_rol' => 1
        ]);

        $categoria = [
            'name' => 'prueba'
        ];

        $modCategoria = [
            'name' => 'modificacion'
        ];

        $token = $user->createToken('id')->accessToken;
        $headers = ['Authorization' => "Bearer ".$token];

        $this->json('get', 'api/categorias', [], $headers)->assertStatus(200);
        $this->json('get', 'api/categorias/1', [], $headers)->assertStatus(200);
        $this->json('post', 'api/categorias', $categoria, $headers)->assertStatus(201);
        $this->json('put', 'api/categorias/6', $modCategoria, $headers)->assertStatus(200);
        $this->json('delete', 'api/categorias/6', [], $headers)->assertStatus(200);
        $this->json('get', 'api/logout', [], $headers)->assertStatus(200);
    }
}
