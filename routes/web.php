<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'IntroController@index')->name('intro');
Route::redirect('register', 'login', 301);

Route::group(['middleware' => 'admin'], function() {

    Route::get('/add-pedido', 'PedidosController@create')->name('add-pedido');
    Route::post("/add-pedido","PedidosController@store")->name('add-pedido');
    Route::get('/mod-pedido/{id}', 'PedidosController@edit')->name('mod-pedido');
    Route::post('/mod-pedido', 'PedidosController@update')->name('mod-pedido');
    Route::delete("/del-pedido/{id}","PedidosController@destroy")->name('del-pedido');

    Route::get('/add-lin-pedido/{id}', 'LinPedController@create')->name('add-lin-pedido');
    Route::post("/add-lin-pedido","LinPedController@store")->name('add-lin-pedido');
    Route::get('/mod-lin-pedido/{id}', 'LinPedController@edit')->name('mod-lin-pedido');
    Route::post('/mod-lin-pedido', 'LinPedController@update')->name('mod-lin-pedido');
    Route::delete("/del-lin-pedido/{id}","LinPedController@destroy")->name('del-lin-pedido');

    Route::get('/add-producto', 'ProductosController@create')->name('add-producto');
    Route::post("/add-producto","ProductosController@store")->name('add-producto');
    Route::get('/mod-producto/{id}', 'ProductosController@edit')->name('mod-producto');
    Route::post('/mod-producto', 'ProductosController@update')->name('mod-producto');
    Route::delete("/del-producto/{id}","ProductosController@destroy")->name('del-producto');

    Route::get('/add-usuario', 'UserController@create')->name('add-user');
    Route::post("/add-usuario","UserController@store")->name('add-user');
    Route::get('/mod-usuario/{id}', 'UserController@edit')->name('mod-user');
    Route::post('/mod-usuario', 'UserController@update')->name('mod-user');
    Route::delete("/del-usuario/{id}","UserController@destroy")->name('del-user');

    Route::get('/add-dir-user/{id}', 'DireccionController@create')->name('add-dir-user');
    Route::post("/add-dir-user","DireccionController@store")->name('add-dir-user');
    Route::get('/mod-dir-user/{id}', 'DireccionController@edit')->name('mod-dir-user');
    Route::post('/mod-dir-user', 'DireccionController@update')->name('mod-dir-user');
    Route::delete("/del-dir-user/{id}","DireccionController@destroy")->name('del-dir-user');

    Route::get('/add-prov', 'ProveedoresController@create')->name('add-prov');
    Route::post("/add-prov","ProveedoresController@store")->name('add-prov');
    Route::get('/mod-prov/{id}', 'ProveedoresController@edit')->name('mod-prov');
    Route::post('/mod-prov', 'ProveedoresController@update')->name('mod-prov');
    Route::delete("/del-prov/{id}","ProveedoresController@destroy")->name('del-prov');

    Route::get('/add-cat', 'CategoriaController@create')->name('add-cat');
    Route::post("/add-cat","CategoriaController@store")->name('add-cat');
    Route::get('/mod-cat/{id}', 'CategoriaController@edit')->name('mod-cat');
    Route::post('/mod-cat', 'CategoriaController@update')->name('mod-cat');
    Route::delete("/del-cat/{id}","CategoriaController@destroy")->name('del-cat');

    Route::get('/add-estado', 'EstadoController@create')->name('add-estado');
    Route::post("/add-estado","EstadoController@store")->name('add-estado');
    Route::get('/mod-estado/{id}', 'EstadoController@edit')->name('mod-estado');
    Route::post('/mod-estado', 'EstadoController@update')->name('mod-estado');
    Route::delete("/del-estado/{id}","EstadoController@destroy")->name('del-estado');

    Route::get('/export-excel', 'ProductosController@export')->name('export');
    Route::post('/import-excel', 'ProductosController@import')->name('import');

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/pedidos', 'PedidosController@index')->name('pedidos');
    Route::get('/productos', 'ProductosController@index')->name('productos');
    Route::get('/usuarios', 'UserController@index')->name('usuarios');
    Route::get('/proveedores', 'ProveedoresController@index')->name('proveedores');
    Route::get('/categorias', 'CategoriaController@index')->name('categorias');
    Route::get('/estados', 'EstadoController@index')->name('estados');
    Route::get('/cuenta', 'UserController@show')->name('cuenta');

});

Route::group(['middleware' => 'repartidor'], function() {
    Route::get('/pedidos-pdf-show/{id}', 'PedidosController@convert')->name('convert');
    Route::get('/pedidos-pdf-descargar/{id}', 'PedidosController@pdf')->name('pdf');
});