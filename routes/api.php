<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::resource('productos', 'Api\ProductosController');
Route::resource('categorias', 'Api\CategoriaController');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('logout', 'AuthController@logout');
    Route::get('user', 'AuthController@user');
    Route::resource('direcciones', 'Api\DireccionController');
    Route::resource('pedidos', 'Api\PedidosController');
    Route::put('cambio-estado/{id}', 'Api\PedidosController@cambio');
});

Route::group(['middleware' => ['auth:api', 'repartidor']], function() {
    Route::resource('estados', 'Api\EstadoController');
    Route::resource('users', 'Api\UserController');
    Route::get('pedidos-repartidor', 'Api\PedidosController@repartidor');});

