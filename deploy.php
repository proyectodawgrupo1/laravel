<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'batoilogic-backend');

// Project repository
set('repository', 'git@bitbucket.org:proyectodawgrupo1/laravel.git');
set('keep_releases', 1);
// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 
set('default_timeout', 0);
// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts

host('3.81.22.76')
    ->stage('staging')
    ->user('laravel-staging')
    ->port(443)
    ->set('branch', 'staging')
    ->set('deploy_path', '/var/www/laravel');

host('52.90.204.36')
    ->stage('produccion')
    ->user('laravel-produccion')
    ->port(443)
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/laravel');


// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

desc('Deploy your project');
task('deploy', [
'deploy:info',
'deploy:prepare',
'deploy:lock',
'deploy:release',
'deploy:update_code',
'deploy:shared',
'deploy:vendors',
'deploy:writable',
'artisan:storage:link',
'artisan:view:cache',
'artisan:config:cache',
'deploy:symlink',
'deploy:unlock',
'cleanup',
]);

task('reload:php-fpm', function () {
run('sudo /etc/init.d/php7.2-fpm restart');
});

task('clear:cache', function () {
run('cd /var/www/laravel/current && php artisan cache:clear');
run('cd /var/www/laravel/current && php artisan view:clear');
run('cd /var/www/laravel/current && php artisan view:clear');
run('cd /var/www/laravel/current && php artisan clear-compiled');
run('cd /var/www/laravel/current && php artisan config:cache');
});


after('deploy', 'reload:php-fpm');
after('deploy', 'clear:cache');
