<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ResourceProducto extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name, 
            'categoria' => $this->cat,
            'descripcion_1' => $this->descripcion_1, 
            'descripcion_2' => $this->descripcion_2, 
            'precio' => $this->precio,  
            'image' => $this->image,
            'proveedor' => $this->prov,
            'stock' => $this->stock           
        ];
    }
}
