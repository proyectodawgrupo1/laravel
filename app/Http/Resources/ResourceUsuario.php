<?php

namespace App\Http\Resources;

use App\Pedido;
use App\Http\Resources\ResourcePedido;
use App\Http\Resources\ResourceDireccion;
use Illuminate\Http\Resources\Json\JsonResource;

class ResourceUsuario extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email, 
            'rol' => $this->rol,
            'direcciones' => ResourceDireccion::collection($this->direcciones),
            'pedidos' => ResourcePedido::collection(Pedido::where('id_cliente',$this->id)->get())   
        ];
    }
}
