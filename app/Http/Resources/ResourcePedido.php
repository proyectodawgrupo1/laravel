<?php
namespace App\Http\Resources;

use App\LinPed;
use App\Direccion;
use App\Http\Resources\ResourceDireccion;
use App\Http\Resources\ResourceLinPedido;
use Illuminate\Http\Resources\Json\JsonResource;

class ResourcePedido extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'referencia' => $this->referencia,
            'cliente' => [
                'id' => $this->id_cliente,
                'name'=> $this->cliente->name
            ],
            'direccion' => Direccion::where('id',$this->id_direccion)->first(),      
            'id_repartidor' => $this->id_repartidor,
            'estado' => $this->estado,
            'lineas' => $this->lineas               
        ];
    }
}
