<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ResourceDireccion extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'id_user' => $this->id_user, 
            'name' => $this->name,
            'direccion_1' => $this->direccion_1,   
            'direccion_2' => $this->direccion_2,   
            'ciudad' => $this->ciudad,  
            'provincia' => $this->provincia, 
            'pais' => $this->pais, 
            'telefono' => $this->telefono          
        ];
    }
}
