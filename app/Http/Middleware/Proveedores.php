<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Proveedores
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->id_rol == 1 || Auth::user()->id_rol == 2) {
            return $next($request);
    }else{
        return response()->json([
            'message' => 'No tienes suficientes permisos'], 401);
    }
    }
}
