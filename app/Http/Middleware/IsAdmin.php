<?php
namespace App\Http\Middleware;

use Auth;
use Closure;

class IsAdmin
{
    /**
     * Comprueba que solo pueden acceder administradores.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() &&  Auth::user()->id_rol == 1) {
                return $next($request);
        }else{
            return response()->json([
                'message' => 'No tienes suficientes permisos'], 401);
        }
    }
}
