<?php

namespace App\Http\Controllers\Api;

use App\Direccion;
use App\Http\Resources\ResourceDireccion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DireccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('repartidor');

        return ResourceDireccion::collection(Direccion::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_user' => 'required|integer',
            'name' => 'required|string',
            'direccion_1' => 'required|string',
            'direccion_2' => 'required|string',
            'ciudad' => 'required|string',
            'provincia' => 'required|string',
            'pais' => 'required|string',
            'telefono' => 'required|string'
        ]);

        $direccion = new Direccion([
            'id_user' => $request->id_user,
            'name' => $request->name,
            'direccion_1' => $request->direccion_1,
            'direccion_2' => $request->direccion_2,
            'ciudad' => $request->ciudad,
            'provincia' => $request->provincia,
            'pais' => $request->pais,
            'telefono' => $request->telefono
        ]);
        
        $direccion->save();

        return $direccion;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->middleware('repartidor');

        return new ResourceDireccion(Direccion::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_user' => 'required|integer',
            'name' => 'required|string',
            'direccion_1' => 'required|string',
            'direccion_2' => 'required|string',
            'ciudad' => 'required|string',
            'provincia' => 'required|string',
            'pais' => 'required|string',
            'telefono' => 'required|string'
        ]);

        Direccion::where('id', $id)->update([
            'id_user' => $request->id_user,
            'name' => $request->name,
            'direccion_1' => $request->direccion_1,
            'direccion_2' => $request->direccion_2,
            'ciudad' => $request->ciudad,
            'provincia' => $request->provincia,
            'pais' => $request->pais,
            'telefono' => $request->telefono
        ]);

        return Direccion::where('id', $id)->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Direccion::findOrFail($id)->delete();
        return $id;
    }
}
