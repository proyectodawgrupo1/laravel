<?php

namespace App\Http\Controllers\Api;

use App\Pedido;
use App\LinPed;
use App\User;
use App\Producto;
use App\Http\Resources\ResourcePedido;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('repartidor');

        return ResourcePedido::collection(Pedido::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_repartidor' => 'required|integer',
            'id_cliente' => 'required|integer',
            'id_direccion' => 'required|integer',
        ]);

        if($request->id_repartidor==0){
            $randRep = User::where('id_rol',2)->get()->random(1)->first(); 
            $request->id_repartidor = $randRep->id;
        }

        $pedido = new Pedido([
            'referencia' => $this->generateRandomString(25),
            'id_repartidor' => $request->id_repartidor,
            'id_cliente' => $request->id_cliente,
            'id_estado' => 1,
            'id_direccion' => $request->id_direccion,
        ]);

        $pedido->save();

        if($request->lineas){
            foreach($request->lineas as $linea){
                $newLinPed = new LinPed([
                    'id_pedido' => $pedido->id,
                    'id_producto' => $linea['producto']['id'], //se trata del id del producto, en la app Cliente
                    'cantidad' => $linea['cantidad']
                ]);
                $newLinPed->save();
                $producto = Producto::where('id',$linea['producto']['id'])->first();
                Producto::where('id', $producto->id)->update([
                    'stock' => $producto->stock - $linea['cantidad']
                ]);
                $producto->save();
            }
        }
        return new ResourcePedido($pedido);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->middleware('repartidor');
        return new ResourcePedido(Pedido::find($id));
    }

    /**
     * Cambia de estado un pedido en concreto.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cambio(Request $request, $id)
    {
        $request->validate([
            'id_estado' => 'required|integer'
        ]);

        $pedido = Pedido::where('id', $id)->update([
            'id_estado' => $request->id_estado
        ]);

        if($request->id_estado==6){
            $pedido = Pedido::where('id',$id)->first();
            foreach($pedido->lineas as $linea){
                $producto = Producto::where('id',$linea->id_producto)->first();
                Producto::where('id', $producto->id)->update([
                    'stock' => $producto->stock += $linea->cantidad
                ]);
                $producto->save();
            }
            $pedido->save();
        }

        return new ResourcePedido(Pedido::find($id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function repartidor()
    {
        $this->middleware('repartidor');
        return ResourcePedido::collection(Pedido::where('id_repartidor',Auth::user()->id)->get()); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_repartidor' => 'required|integer',
            'id_cliente' => 'required|integer',
            'id_direccion' => 'required|integer',
            'id_estado' => 'required|integer'
        ]);

        Pedido::where('id', $id)->update([
            'id_repartidor' => $request->id_repartidor,
            'id_cliente' => $request->id_cliente,
            'id_direccion' => $request->id_direccion,
            'id_estado' => $request->id_estado
        ]);

        return response()->json([
            'message' => 'Pedido modificado correctamente'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pedido::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Pedido eliminado correctamente'], 200);
    }

    /**
     * Remove referencia number.
     *
     * @param  int  $length
     * @return \Illuminate\Http\Response
     */
    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
