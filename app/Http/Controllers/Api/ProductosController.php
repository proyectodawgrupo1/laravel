<?php

namespace App\Http\Controllers\Api;

use App\Producto;
use App\Http\Resources\ResourceProducto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourceProducto::collection(Producto::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'categoria' => 'required|integer',
            'descripcion_1' => 'required|string',
            'descripcion_2' => 'required|string',
            'precio' => 'required|string',
            'image' => 'required|string',
            'proveedor' => 'required|integer',
            'stock' => 'required|integer'
        ]);

        $producto = new Producto([
            'name' => $request->name,
            'categoria' => $request->categoria,
            'descripcion_1' => $request->descripcion_1,
            'descripcion_2' => $request->descripcion_2,
            'precio' => $request->precio,
            'image' => $request->image,
            'proveedor' => $request->proveedor,
            'stock' => $request->stock
        ]);
        $producto->save();

        return response()->json([
            'message' => 'Producto creado correctamente'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ResourceProducto(Producto::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'categoria' => 'required|integer',
            'descripcion_1' => 'required|string',
            'descripcion_2' => 'required|string',
            'precio' => 'required|string',
            'image' => 'required|string',
            'proveedor' => 'required|integer',
            'stock' => 'required|integer'
        ]);

        Producto::where('id', $id)->update([
            'name' => $request->name,
            'categoria' => $request->categoria,
            'descripcion_1' => $request->descripcion_1,
            'descripcion_2' => $request->descripcion_2,
            'precio' => $request->precio,
            'image' => $request->image,
            'proveedor' => $request->proveedor,
            'stock' => $request->stock
        ]);

        return response()->json([
            'message' => 'Producto modificado correctamente'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Producto::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Producto eliminado correctamente'], 200);
    }
}
