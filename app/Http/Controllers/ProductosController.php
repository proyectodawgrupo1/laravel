<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Proveedor;
use App\Categoria;
use App\Exports\ProductsExport;
use App\Imports\ProductsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Api\ProductosController as api;

class ProductosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->api = new api;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $productos = $this->api->index();
        return view("productos.productos",compact('productos'));
    }

    /**
     * Export products to excel format.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function export()
    {
        return Excel::download(new ProductsExport, 'products.xls');
    }

    /**
     * Import products from excel format.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function import(Request $request)
    {
        $file = $request->file('file');
        Excel::import(new ProductsImport, $file);
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();
        $proveedores = Proveedor::all();
        return view("productos.form-productos",compact('categorias','proveedores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->api->store($request);
        return redirect()->route('productos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::where('id',$id)->first();
        $categorias = Categoria::all();
        $proveedores = Proveedor::all();
        return view("productos.form-productos",compact('id','producto','categorias','proveedores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->api->update($request, $request->id);
        return redirect()->route('productos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->api->destroy($id);
        return redirect()->route('productos');
    }
}
