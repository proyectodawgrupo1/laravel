<?php

namespace App\Http\Controllers;

use App\User;
use App\Estado;
use App\Pedido;
use App\LinPed;
use App\Direccion;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Api\PedidosController as api;

class PedidosController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->api = new api;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pedidos = $this->api->index();
        return view("pedidos.pedidos",compact('pedidos'));
    }

    /**
     * Crea el albaran de un pedido en concreto a PDF
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function convert($id)
    {
        $pedido = Pedido::where('id',$id)->first();
        return view("pedidos.albaran",compact('pedido'));
    }

    /**
     * Descarga un albaran de un pedido en concreto a PDF
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pdf($id)
    {
        $pedido = Pedido::where('id',$id)->first();
        $pdf = PDF::loadView('pedidos.albaran', compact('pedido'));
        return $pdf->download('albaran-'.$id.'.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = User::where('id_rol',3)->get();
        $repartidores = User::where('id_rol',2)->get();
        $direcciones = Direccion::all();
        return view("pedidos.form-pedidos",compact('clientes','repartidores','direcciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->api->store($request);
        return redirect()->route('pedidos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido = Pedido::where('id',$id)->first();
        $clientes = User::where('id_rol',3)->get();
        $repartidores = User::where('id_rol',2)->get();
        $direcciones = Direccion::where('id_user',User::where('id',$pedido->id_cliente)->first()->id)->get();
        $estados = Estado::all();
        return view("pedidos.form-pedidos",compact('id','pedido','estados','clientes','repartidores','direcciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->api->update($request, $request->id);
        return redirect()->route('pedidos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->api->destroy($id);
        return redirect()->route('pedidos');
    }
}
