<?php

namespace App\Http\Controllers;

use App\User;
use App\Direccion;
use Illuminate\Http\Request;

class DireccionController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $user = User::where('id',$id)->first();
        return view("usuarios.usuarios-dirs",compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_user' => 'required|integer',
            'name' => 'required|string',
            'direccion_1' => 'required|string',
            'direccion_2' => 'required|string',
            'ciudad' => 'required|string',
            'provincia' => 'required|string',
            'pais' => 'required|string',
            'telefono' => 'required|string'
        ]);

        $dir = new Direccion([
            'id_user' => $request->id_user,
            'name' => $request->name,
            'direccion_1' => $request->direccion_1,
            'direccion_2' => $request->direccion_2,
            'ciudad' => $request->ciudad,
            'provincia' => $request->provincia,
            'pais' => $request->pais,
            'telefono' => $request->telefono
        ]);
        
        $dir->save();
        return redirect()->route('usuarios');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $direccion = Direccion::where('id',$id)->first();
        return view("usuarios.usuarios-dirs",compact('id','direccion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'id_user' => 'required|integer',
            'name' => 'required|string',
            'direccion_1' => 'required|string',
            'direccion_2' => 'required|string',
            'ciudad' => 'required|string',
            'provincia' => 'required|string',
            'pais' => 'required|string',
            'telefono' => 'required|string'
        ]);
        
        Direccion::where('id', $request->id)->update([
            'id_user' => $request->id_user,
            'name' => $request->name,
            'direccion_1' => $request->direccion_1,
            'direccion_2' => $request->direccion_2,
            'ciudad' => $request->ciudad,
            'provincia' => $request->provincia,
            'pais' => $request->pais,
            'telefono' => $request->telefono
        ]);
        return redirect()->route('usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Direccion::findOrFail($id)->delete();
        return redirect()->route('usuarios');
    }
}
