<?php

namespace App\Http\Controllers;

use App\Pedido;
use App\Producto;
use App\LinPed;
use Illuminate\Http\Request;

class LinPedController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $productos = Producto::all();
        $pedido = Pedido::where('id',$id)->first();
        return view("pedidos.form-linped",compact('pedido','productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pedido' => 'required|integer',
            'id_producto' => 'required|integer',
            'cantidad' => 'required|integer'
        ]);

        $linPed = new LinPed([
            'id_pedido' => $request->id_pedido,
            'id_producto' => $request->id_producto,
            'cantidad' => $request->cantidad
        ]);
        $linPed->save();

        $producto = Producto::where('id',$request->id_producto)->first();
        Producto::where('id', $producto->id)->update([
            'stock' => $producto->stock -= $request->cantidad
        ]);
        $producto->save();

        return redirect()->route('pedidos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $linPed = LinPed::where('id',$id)->first();
        $productos = Producto::all();
        return view("pedidos.form-linped",compact('id','linPed','productos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'id_pedido' => 'required|integer',
            'id_producto' => 'required|integer',
            'cantidad' => 'required|integer'
        ]);
        
        LinPed::where('id', $request->id)->update([
            'id_pedido' => $request->id_pedido,
            'id_producto' => $request->id_producto,
            'cantidad' => $request->cantidad
        ]);

        $producto = Producto::where('id',$request->id_producto)->first();
        $linea = LinPed::where('id',$request->id)->first();

        if($linea->cantidad > $request->cantidad){
            Producto::where('id', $producto->id)->update([
                'stock' => $producto->stock += $request->cantidad
            ]);
        }else{
            Producto::where('id', $producto->id)->update([
                'stock' => $producto->stock -= $request->cantidad
            ]);
        }

        $producto->save();

        return redirect()->route('pedidos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LinPed::findOrFail($id)->delete();
        return redirect()->route('pedidos');
    }
}
