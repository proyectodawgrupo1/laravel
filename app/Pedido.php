<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model {

    protected $table = 'pedidos';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'referencia',
        'id_repartidor',
        'id_cliente',
        'id_estado',
        'id_direccion'
    ];

    public function repartidor()
    {
        return $this->belongsTo(User::class,'id_repartidor');
    }

    public function cliente()
    {
        return $this->belongsTo(User::class,'id_cliente');
    }

    public function estado()
    {
        return $this->belongsTo(Estado::class,'id_estado');
    }

    public function direccion()
    {
        return $this->belongsTo(Direccion::class,'id_direccion');
    }

    public function lineas()
    {
        return $this->hasMany(LinPed::class,'id_pedido');
    }

}
