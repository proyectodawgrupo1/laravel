<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinPed extends Model
{
    protected $table = 'lin_pedido';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'id_pedido',
        'id_producto',
        'cantidad'
    ];

    public function producto()
    {
        return $this->belongsTo(Producto::class,'id_producto');
    }

}
