<?php

namespace App\Console\Commands;

use App\Producto;
use Illuminate\Console\Command;

class ClearProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elimina todos los productos cuyo stock sea 0';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Producto::where('stock',0)->delete();
        echo "Productos eliminados correctamente\n";
    }
}
