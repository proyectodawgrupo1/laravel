<?php

namespace App\Console\Commands;

use App\Pedido;
use Illuminate\Console\Command;

class ClearPedidos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:pedidos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elimina todos los pedidos que ya hayan sido entregados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Pedido::where('id_estado',4)->delete();
        echo "Pedidos completados correctamente\n";
    }
}
