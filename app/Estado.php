<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model {

    protected $table = 'estados';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = ['name'];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at','created_at'
    ];

}
