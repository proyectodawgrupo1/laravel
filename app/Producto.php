<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model {

    protected $table = 'productos';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'categoria',
        'descripcion_1',
        'descripcion_2',
        'precio',
        'image',
        'proveedor',
        'stock'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at','created_at'
    ];

    public function cat()
    {
        return $this->belongsTo(Categoria::class,'categoria');
    }

    public function prov()
    {
        return $this->belongsTo(Proveedor::class,'proveedor');
    }

}
