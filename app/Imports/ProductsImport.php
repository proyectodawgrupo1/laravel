<?php

namespace App\Imports;

use App\Producto;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Producto([
            'name' => $row[0],
            'categoria' => $row[1],
            'descripcion_1' => $row[2],
            'descripcion_2' => $row[3],
            'precio' => $row[4],
            'image' => $row[5],
            'proveedor' => $row[6],
            'stock' => $row[7]
        ]);
    }
}
